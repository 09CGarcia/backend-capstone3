const User = require("./../models/User");
const Program = require("./../models/Programs");
const bcrypt = require('bcrypt');
const auth = require("./../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then((result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	})

}

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 7)
	});

	return newUser.save().then((res, err) => {
		if(err){
			return err
		} else {
			return true
		}
	});
};

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {
		if(result === null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect === true){
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}
module.exports.getProfile = (data) => {
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.enroll = async (data) => {

	console.log(data)
	const userSaveStatus = await User.findById(data.userId).then( user => {
		
		user.enrollments.push({programId: data.programId, 
							   name: data.name,
							description: data.description,
							price: data.price})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	const programSaveStatus = await Program.findById(data.programId).then( program => {
		program.enrollees.push({userId: data.userId})

		return program.save().then( (program, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	if(userSaveStatus && programSaveStatus){
		return true
	} else {
		return false
	}
}
