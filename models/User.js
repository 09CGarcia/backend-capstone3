const mongoose = require('mongoose');
const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First Name is required."]
		},
		lastName: {
			type: String,
			required: [true, "Last Name is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile Number is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		enrollments: [
			{
				programId: {
					type: String,
					required:  [true, "Program ID is required."]
				},
				name: {
					type: String,
					required: [true, "Name is required."]
				},
				description: {
					type: String,
					required: [true, "Description is required."]
				},
				price: {
					type: String,
					required: [true, "Price is required."]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled"
				}
			}
		]
	}
);

module.exports = mongoose.model("User", userSchema);